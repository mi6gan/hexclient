using UnityEngine;  
using System.Runtime.InteropServices;

public class HexSocket : MonoBehaviour {  
    #if UNITY_WEBGL && !UNITY_EDITOR
    [DllImport("__Internal")]
    public static extern void HexSocketEmit(string method, string data);
    [DllImport("__Internal")]
    public static extern void HexSocketOn(string method, Action<string> listener);
    #endif
}
