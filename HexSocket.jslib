var HexSocket {
    $HexSocketStore: {},
    HexSocketEmit: function(method, data){
        var method = Pointer_stringify(method),
            data = Pointer_stringify(data);
        window.hexSocket.emit(method, data);
    }
    HexSocketOn: function(method, listener){
        window.hexSocket.on(method, function(data){
            HexSocketStore.onListener = listener(data);
            Runtime.dynCall('v', obj, 0);
        });
    }
}
mergeInto(LibraryManager.library, HexSocket);
