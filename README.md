На http://hexes.pro:8080/ помимо билда игры
загружаются js-код, в котором я сделал
класс-враппер для socket.io. Там два метода:
hexSocket#on 
hexSocket#emit

Пример использования в консоли google chrome: 
```
hexSocket.on('game-created', (data)=>{console.log(data);});
hexSocket.emit('game-create');
```

Результат:
```
{
    "url":"http://0.0.0.0:8000/game/5835cf86e35689002a34b249",
    "id":"5835cf86e35689002a34b249"
}
```

Что в данном репозитории:

    HexSocket.jslib примерный код для jslib-плагина
    HexSocket.cs примерный код ассета для работы с плагином
